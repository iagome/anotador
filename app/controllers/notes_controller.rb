class NotesController < ApplicationController
  before_action :set_note, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    # Faz a busca pelo parâmetro procurado ou mostra todas as anotações do usuário logado
    unless params["Buscar"].blank?
      busca = params["Buscar"]
      @notes = Note.where("title ilike '%#{busca}%' OR note ilike '%#{busca}%'").where(user_id: current_user[:id]).order("notes.title DESC")
    else
      @notes = Note.where(user_id: current_user[:id])
    end
  end

  def show
  end

  def new
    @note = Note.new
  end

  def edit
  end

  def create
    @note = Note.new(
      title: note_params["title"],
      note: note_params["note"],
      date: note_params["date"],
      priority: note_params["priority"],
      user_id: current_user[:id]
    )

    respond_to do |format|
      if @note.save
        format.html { redirect_to @note, notice: 'Note was successfully created.' }
        format.json { render :show, status: :created, location: @note }
      else
        format.html { render :new }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @note.update(note_params)
        format.html { redirect_to @note, notice: 'Note was successfully updated.' }
        format.json { render :show, status: :ok, location: @note }
      else
        format.html { render :edit }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @note.destroy
    respond_to do |format|
      format.html { redirect_to notes_url, notice: 'Note was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_note
      @note = Note.find(params[:id])
    end

    def note_params
      params.require(:note).permit(:title, :note, :date, :priority)
    end
end
