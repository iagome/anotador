json.extract! note, :id, :title, :note, :date, :priority, :created_at, :updated_at
json.url note_url(note, format: :json)
