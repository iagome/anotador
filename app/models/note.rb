class Note < ApplicationRecord
  validates :title, :note, :date, :presence => true
  
  belongs_to :user
end
