class CreateNotes < ActiveRecord::Migration[5.1]
  def change
    create_table :notes do |t|
      t.text :title
      t.text :note
      t.datetime :date
      t.integer :priority

      t.timestamps
    end
  end
end
