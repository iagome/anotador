# Anotações online

## Descrição

Sistema que permite ao usuário criar, editar e deletar anotações.
Apenas usuários que possuem um cadastro e estão logados podem efetuar ações.

## Ferramentas Utilizadas

**Back-end:**
* [Ruby 2.3.8](https://www.ruby-lang.org/pt/) - Linguagem principal
* [Ruby on Rails 5.1.5](https://rubyonrails.org/) - Framework Web

**Banco de Dados:**
* [PostgreSQL](https://www.postgresql.org/)

**Front-end:**
* HTML/ERB - Páginas desenvolvidas
* CSS - Estilização das páginas

**Gems:**
* [Devise](https://github.com/heartcombo/devise) - Utilizado para fazer toda autenticação de usuários
* [BootStrap](https://getbootstrap.com/) - Utilizado para estilização do APP 

## Autor

* **Iago Mollon Esqueisaro** - [LinkedIn](https://www.linkedin.com/in/iago-mollon-esqueisaro-820968166/)